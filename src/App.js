import React, { lazy, Suspense } from 'react';
import './App.css';
import { Route, Redirect, Switch } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import LazyLoader from './components/LazyLoader/LazyLoader';

function App() {

  const Home = lazy(() => import('./components/Home/Home'));
  const Login = lazy(() => import('./components/Login/Login'));
  const SignUp = lazy(() => import('./components/SignUp/SignUp'));
  const DashboardSideNav = lazy(() => import('./components/DashboardSideNav/DashboardSideNav'));
  const NotFound = lazy(() => import('./components/NotFound/NotFound'));
  const LoginSignUpOtpService = lazy(() => import('./components/Services/LoginSignUpOtpService/LoginSignUpOtpService'));
  const Configuartions = lazy(() => import('./components/Configurations/Configurations'));

  return (
    <div>
      <CssBaseline />
      <Suspense fallback={LazyLoader()}>
        <Switch>
          <Route exact path="/">
            <Redirect to="/home" />
          </Route>
          <Route exact path="/home" component={Home} />
          <Route exact path="/login">
            <LoginSignUpOtpService>
              {(myLoginHandler, getOtpHandler, mySignUpHandler) => (
                <Login myLoginHandler={myLoginHandler} getOtpHandler={getOtpHandler} mySignUpHandler={mySignUpHandler} />
              )}
            </LoginSignUpOtpService>
          </Route>
          <Route exact path="/signUp">
            <LoginSignUpOtpService>
              {(myLoginHandler, getOtpHandler, mySignUpHandler) => (
                <SignUp myLoginHandler={myLoginHandler} getOtpHandler={getOtpHandler} mySignUpHandler={mySignUpHandler} />
              )}
            </LoginSignUpOtpService>
          </Route>
          <Route path="/dashboard">
            <DashboardSideNav>
              <NotFound />
            </DashboardSideNav>
          </Route>
          <Route exact path="/configurations">
            <Configuartions />
          </Route>
          <Route component={NotFound} />
        </Switch>
      </Suspense>
    </div>
  );
}

export default App;
