import React, { useState, useEffect } from 'react';
import "./StudentMarks.css";
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import { useHistory, useLocation } from "react-router-dom";
import Snackbar from '../../../SnackBar/SnackBar';
import Avatar from 'react-avatar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

function StudentMarks() {

    let history = useHistory();
    let location = useLocation();

    const [studentMarksData, setstudentMarksData] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: false });

    const [open, setOpen] = React.useState(false);

    const [examData, setexamData] = useState({
        examName: ''
    });

    const [examNameList, setexamNameList] = useState(null);

    const [examDTO, setexamDTO] = useState(null);

    useEffect(() => {

        console.log(history, location);

        var examDto = {
            "instituteId": localStorage.getItem('InstituteId')
        }

        axios.post(baseUrl + '/exam/list', examDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setexamNameList(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setexamNameList(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setexamNameList(null);
                handleClick();
            })

    }, []);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const fetchMarks = () => {

        var studentMarksDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "classId": location.state.data.classDTO.id,
            "sectionId": location.state.data.sectionDTO.id,
            "studentId": location.state.data.id,
            "examId": examDTO.id
        }

        axios.post(baseUrl + '/examMark/student/list', studentMarksDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setstudentMarksData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setstudentMarksData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setstudentMarksData(null);
                handleClick();
            })

    }

    const getExamData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setexamData({ ...examData, examName: e.target.value });
        setexamDTO(examNameList[child.props.id]);
        console.log(examData, examDTO);
    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <h5 style={{ color: "#B99E01" }}><i className="fa fa-clock-o"></i>&nbsp;&nbsp;Student<span style={{ color: "black" }}>Marks</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Exam List</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={examData.examName}
                                onChange={(e, child) => getExamData(e, child)}
                            >
                                {
                                    examNameList === null ? null :
                                        examNameList.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name} >
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl><br /><br /><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                    <Button onClick={fetchMarks} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button>
                    </div>
                </div>
                {studentMarksData === null ? (
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
                            <h5>No Marks Available !!</h5>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                    </div>
                ) :
                    (
                        <div>
                            <div className="row">
                                {studentMarksData.examMarkList.map((data, index) => (
                                <div key={data.examMarkId} className="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                                <Avatar name={data.examDisplayName} size="35" round={true} />
                                            </div>
                                            <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                <h5 style={{ marginTop: "4px" }} className="card-title">{data.examDisplayName}</h5>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                            <div className="col-xs-12 col-sm-12 col-md-11 col-lg-11">
                                                <h5>Subject: {data.subjectName}</h5>
                                                <h5>Total Marks : {data.totalMarks}</h5>
                                                <h5>Pass Marks : {data.scoredMarks}</h5>
                                                {
                                                    data.comments === null ? null :
                                                        <h5>Comments : {data.comments}</h5>
                                                }
                                                <h5>Passed : {data.passed === true ? <span>Yes</span> : <span>No</span>}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div><br />
                            </div>
                                ))}
                            </div>
                        </div>
                    )
                }
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default StudentMarks

