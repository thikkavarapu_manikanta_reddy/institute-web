import React, { useState, useEffect } from 'react';
import "./AddStudents.css";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useHistory, useLocation } from "react-router-dom";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import Switch from '@material-ui/core/Switch';
import Snackbar from '../../../SnackBar/SnackBar';

function AddStudents() {

    let location = useLocation();
    let history = useHistory();

    const [studentsData, setstudentsData] = useState({
        firstName: '',
        lastName: '',
        regNo: '',
        mobileNumber: '',
        address: '',
        fatherName: '',
        motherName: '',
        emergencyContactName: '',
        emergencyContactNo: '',
        class: '',
        section: '',
        autoReg: true,
        DOB: new Date(),
        gender: 'Male'
    });

    const [classDTO, setclassDTO] = useState(null);
    const [sectionDTO, setsectionDTO] = useState(null);

    const [uploadImage, setuploadImage] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    const [classData, setclassData] = useState(null);

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);

    useEffect(() => {
        console.log(history, location);

        var classDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        axios.post(baseUrl + '/class/list', classDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setclassData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setclassData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setclassData(null);
                handleClick();
            })

        if (location.state.studentType === 'EDIT') {
            setstudentsData({
                ...studentsData,
                firstName: location.state.data.firstName,
                lastName: location.state.data.lastName,
                mobileNumber: location.state.data.mobileNumber,
                address: location.state.data.address,
                fatherName: location.state.data.fatherName,
                motherName: location.state.data.motherName,
                emergencyContactName: location.state.data.emergencyContactName,
                emergencyContactNo: location.state.data.emergencyContact,
                class: location.state.data.classDTO.name,
                section: location.state.data.sectionDTO.name,
                autoReg: location.state.data.autoGenerate,
                DOB: new Date(location.state.data.dateOfBirth),
                gender: location.state.data.gender
            });
            setuploadImage(location.state.data.imageUrl);
            setclassDTO(location.state.data.classDTO);
            setsectionDTO(location.state.data.sectionDTO);
        }

    }, []);

    const getstudentsData = (e) => {
        setstudentsData({ ...studentsData, [e.target.name]: e.target.value });
        console.log(studentsData, classDTO, sectionDTO, uploadImage);
    }

    const getStudentsClassData = (e,child) => {
        console.log(e.target.value,child.props.id,child);
        if (studentsData.class !== '') {
            setstudentsData({ ...studentsData, class: e.target.value, section: '' });
            setsectionDTO(null);
        }
        else {
            setstudentsData({ ...studentsData, class: e.target.value });
        }
        setclassDTO(classData.data[child.props.id]);
        classSectionTypeChange();
        console.log(studentsData, classDTO, sectionDTO, uploadImage);
    }

    const getStudentsSectionData = (e,child) => {
        console.log(e.target.value,child.props.id,child);
        setstudentsData({ ...studentsData, section: e.target.value });
        setsectionDTO(classDTO.sections[child.props.id]);
        classSectionTypeChange();
        console.log(studentsData, classDTO, sectionDTO, uploadImage);
    }

    const classSectionTypeChange = () => {

        if(location.state.studentType === "CREATE")
        {
            console.log("CREATED");
            setclassDTO((classDTO) => ({...classDTO,type:"CREATE"}));
            setsectionDTO((sectionDTO) => ({...sectionDTO,type:"CREATE"}));
            console.log(studentsData, classDTO, sectionDTO, uploadImage,"CREATED");
        }
        else if(location.state.studentType === "EDIT")
        {
            console.log("EDITED");
            setclassDTO((classDTO) => ({...classDTO,type:"EDIT"}));
            setsectionDTO((sectionDTO) => ({...sectionDTO,type:"EDIT"}));
            console.log(studentsData, classDTO, sectionDTO, uploadImage,"EDITED");
        }

    }

    const getUploadedImage = (e) => {
        console.log(e, e.target.files);
        setuploadImage(e.target.files[0]);
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const addstudentsSubmit = () => {

        var studentDto = {
            "firstName": studentsData.firstName,
            "lastName": studentsData.lastName,
            "address": studentsData.address,
            "mobileNumber": studentsData.mobileNumber,
            "fatherName": studentsData.fatherName,
            "motherName": studentsData.motherName,
            "emergencyContactName": studentsData.emergencyContactName,
            "emergencyContact": studentsData.emergencyContactNo,
            "classDTO": classDTO,
            "sectionDTO": sectionDTO,
            "autoGenerate": studentsData.autoReg,
            "gender": studentsData.gender,
            "dateOfBirth": studentsData.DOB.getFullYear() + "-" + (studentsData.DOB.getMonth() + 1) + "-" + studentsData.DOB.getDate(),
            "instituteId": localStorage.getItem('InstituteId'),
            "id": location.state.data === null ? null : location.state.data.id,
            "type": location.state.studentType
        }

        let formData = new FormData();
        formData.append('studentDto', JSON.stringify(studentDto));
        formData.append('image', uploadImage);

        console.log(formData, studentDto, JSON.stringify(studentsData), uploadImage);

        axios.post(baseUrl + '/student', formData, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'content-type': 'multipart/form-data', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    setTimeout(function () {
                        history.push(
                            {
                                pathname: '/dashboard/institute/student',
                            }
                        );
                    }, 2000);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })
    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div style={{ backgroundColor: "#eeeeee", borderRadius: "4px" }} className="col-xs-12 col-sm-12 col-md-6 col-lg-6"><br />
                        <h5 style={{ color: "#B99E01", textAlign: "center" }}><i className="fas fa-user-graduate"></i>&nbsp;&nbsp;Student<span style={{ color: "black" }}>Details</span></h5><br />
                        <TextField name="firstName" value={studentsData.firstName} onChange={getstudentsData} style={{ width: "100%" }} label="First Name" /><br /><br />
                        <TextField name="lastName" value={studentsData.lastName} onChange={getstudentsData} style={{ width: "100%" }} label="Last Name" /><br /><br />
                        <TextField name="mobileNumber" value={studentsData.mobileNumber} onChange={getstudentsData} style={{ width: "100%" }} label="Mobile No" /><br /><br />
                        <TextField name="address" value={studentsData.address} onChange={getstudentsData} style={{ width: "100%" }} label="Address" /><br /><br />
                        <TextField name="fatherName" value={studentsData.fatherName} onChange={getstudentsData} style={{ width: "100%" }} label="Father Name" /><br /><br />
                        <TextField name="motherName" value={studentsData.motherName} onChange={getstudentsData} style={{ width: "100%" }} label="Mother Name" /><br /><br />
                        <TextField name="emergencyContactName" value={studentsData.emergencyContactName} onChange={getstudentsData} style={{ width: "100%" }} label="Emergency Contact Name" /><br /><br />
                        <TextField name="emergencyContactNo" value={studentsData.emergencyContactNo} onChange={getstudentsData} style={{ width: "100%" }} label="Emergency Contact No" /><br /><br />
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Class</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={studentsData.class}
                                onChange={(e,child) => getStudentsClassData(e,child)}
                                // inputProps={{
                                //     name: "class"
                                // }}
                            // name="instituteType"
                            >
                                {
                                    classData === null ? null :
                                        classData.data.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name} >
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl><br /><br />
                        {
                            studentsData.class === '' ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Section</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={studentsData.section}
                                            onChange={(e,child) => getStudentsSectionData(e,child)}
                                            // inputProps={{
                                            //     name: "section"
                                            // }}
                                        // name="instituteType"
                                        >
                                            {
                                                classDTO.sections === null ? null :
                                                    classDTO.sections.map((option, index) => (
                                                        <MenuItem id={index} key={option.id} value={option.name}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl><br /><br />
                                </div>
                        }
                        <Switch
                            checked={studentsData.autoReg}
                            onChange={e => setstudentsData({ ...studentsData, autoReg: e.target.checked })}
                            color="primary"
                            name="autoReg"
                            inputProps={{ 'aria-label': 'primary checkbox' }}
                        />
                        {studentsData.autoReg === true ? <span>AutoReg</span> : <TextField name="regNo" value={studentsData.regNo} onChange={getstudentsData} style={{ width: "100%" }} label="RegNo" />}<br /><br />
                        <div className="customDatePickerWidth">
                            <DatePicker dateFormat="yyyy/MM/dd" peekNextMonth showMonthDropdown showYearDropdown dropdownMode="select" selected={studentsData.DOB} onChange={date => setstudentsData({ ...studentsData, DOB: date })} /></div><br />
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Gender</FormLabel>
                            <RadioGroup row aria-label="gender" name="gender" value={studentsData.gender} onChange={getstudentsData}>
                                <FormControlLabel value="Male" control={<Radio color="primary" />} label="Male" />
                                <FormControlLabel value="Female" control={<Radio color="primary" />} label="Female" />
                                <FormControlLabel value="Other" control={<Radio color="primary" />} label="Other" />
                            </RadioGroup>
                        </FormControl><br /><br />
                        <Button
                            variant="contained"
                            component="label"
                            color="secondary"
                        >
                            Upload Image
                            <input
                                type="file"
                                style={{ display: "none" }}
                                onChange={getUploadedImage}
                            />
                        </Button>
                        {uploadImage !== null ? <span>&nbsp;&nbsp;<i style={{ color: "green" }} className="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Successfully Uploaded !!</span> : null}
                        <br /><br />
                        <Button disabled={
                            studentsData.firstName === '' ||
                            studentsData.lastName === '' ||
                            studentsData.address === '' ||
                            studentsData.mobileNumber === '' ||
                            studentsData.fatherName === '' ||
                            studentsData.motherName === '' ||
                            studentsData.emergencyContactName === '' ||
                            studentsData.emergencyContactNo === '' ||
                            studentsData.class === '' ||
                            studentsData.section === '' ||
                            uploadImage === null
                        } onClick={addstudentsSubmit} style={{ fontSize: "100%" }} variant="contained" color="primary">Submit</Button><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default AddStudents

