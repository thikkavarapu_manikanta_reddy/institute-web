import React, { useState, useEffect } from 'react';
import "./HomeWorkDashboard.css";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useHistory, useLocation } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import Snackbar from '../../../SnackBar/SnackBar';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import $ from 'jquery';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';

function HomeWorkDashboard() {

    const contentTypes = [
        {
            "id": "1",
            "name": "IMAGE"
        },
        {
            "id": "2",
            "name": "AUDIO"
        },
        {
            "id": "3",
            "name": "VIDEO"
        },
        {
            "id": "4",
            "name": "PDF"
        }
    ];

    let location = useLocation();
    let history = useHistory();

    const [homeWorkDate, sethomeWorkDate] = useState(new Date());

    const [subjectData, setsubjectData] = useState({
        subjectName: ''
    });

    const [subjectNameList, setsubjectNameList] = useState(null);

    const [classData, setclassData] = useState({
        class: '',
        section: ''
    });

    const [classDTO, setclassDTO] = useState(null);
    const [sectionDTO, setsectionDTO] = useState(null);
    const [subjectDTO, setsubjectDTO] = useState(null);

    const [classListData, setclassListData] = useState(null);


    const [homeWorkData, sethomeWorkData] = useState({
        homeWorkDescription: ''
    });

    const [homeWorkList, sethomeWorkList] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);

    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    const [homeWorkContentData, sethomeWorkContentData] = useState({
        description: '',
        contentName: '',
        contentType: ''
    });

    const [contentFileDataList, setcontentFileDataList] = useState([]);

    const [fileData, setfileData] = useState(null);

    const [editState, seteditState] = useState({
        status: false,
        index: -1
    });

    const [acceptContentType, setacceptContentType] = useState(".jpeg,.jpg,.png,.mp4,.mp3,.pdf");

    const [mediaFiles, setmediaFiles] = useState({});

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    const gethomeWorkContentData = (e) => {
        sethomeWorkContentData({ ...homeWorkContentData, [e.target.name]: e.target.value });
        console.log(e.target.value, e.target.name);
        if (e.target.name === "contentType") {
            if (e.target.value === "IMAGE") {
                setacceptContentType(".jpeg,.jpg,.png");
            }
            else if (e.target.value === "AUDIO") {
                setacceptContentType(".mp3,.wav");
            }
            else if (e.target.value === "VIDEO") {
                setacceptContentType(".mp4");
            }
            else {
                setacceptContentType(".pdf");
            }
        }
        setTimeout(function () { sethomeWorkContentData(prevhomeWorkContentData => ({ ...prevhomeWorkContentData, contentName: prevhomeWorkContentData.contentName, contentType: prevhomeWorkContentData.contentType })); }, 0);
        console.log(homeWorkContentData, contentFileDataList, mediaFiles, fileData, acceptContentType);
    }

    const uploadSyllabusContent = () => {

        if (editState.status === false) {
            setcontentFileDataList([...contentFileDataList, {
                "priority": contentFileDataList.length + 1,
                "name": homeWorkContentData.contentName,
                "type": homeWorkContentData.contentType,
                "dtoType": "CREATE",
                "view": true
            }]);
            console.log(contentFileDataList.length);
            setmediaFiles({ ...mediaFiles, ["CONTENT-" + (contentFileDataList.length + 1)]: fileData });

        }
        else {
            let newArr = [...contentFileDataList];
            newArr[editState.index].name = homeWorkContentData.contentName;
            newArr[editState.index].type = homeWorkContentData.contentType;
            newArr[editState.index].view = false;
            setcontentFileDataList(newArr);
            seteditState({ ...editState, status: false, index: -1 });
            setmediaFiles({ ...mediaFiles, ["CONTENT-" + newArr[editState.index].priority]: fileData });
        }

        setfileData(null);
        document.getElementById('customFile').value = "";
        sethomeWorkContentData({ ...homeWorkContentData, contentName: '', contentType: '' });

        console.log(contentFileDataList, mediaFiles);

    }

    const uploadFile = (e) => {

        console.log(e.target.files.length);
        if (e.target.files.length === 1) {
            console.log(e.target.files, "cool", e.target.files[0].name);
            setfileData(e.target.files[0]);
            console.log(fileData);
        }
        else {
            setfileData(null);
            document.getElementById('customFile').value = "";
            var fileName = "Choose file";
            $(".custom-file-input").siblings(".custom-file-label").addClass("selected").html(fileName);
        }

    }

    const viewMediaFiles = (data) => {
        window.open(data.value);
    }

    const editMediaFiles = (data, index) => {

        sethomeWorkContentData({ ...homeWorkContentData, contentName: data.name, contentType: data.type });
        seteditState({ ...editState, status: true, index: index });

        if (data.type === "IMAGE") {
            setacceptContentType(".jpeg,.jpg,.png");
        }
        else if (data.type === "AUDIO") {
            setacceptContentType(".mp3,.wav");
        }
        else if (data.type === "VIDEO") {
            setacceptContentType(".mp4");
        }
        else {
            setacceptContentType(".pdf");
        }

        setTimeout(function () { sethomeWorkContentData(prevhomeWorkContentData => ({ ...prevhomeWorkContentData, contentName: prevhomeWorkContentData.contentName, contentType: prevhomeWorkContentData.contentType })); }, 0);

    }

    const removeEditMode = () => {
        seteditState({ ...editState, status: false, index: -1 });
        setfileData(null);
        document.getElementById('customFile').value = "";
        sethomeWorkContentData({ ...homeWorkContentData, contentName: '', contentType: '' });
        console.log(contentFileDataList, mediaFiles);
    }

    useEffect(() => {
        console.log(history, location);

        if (roleStatus !== 'STUDENT') {
            var classDto = {
                "instituteId": localStorage.getItem('InstituteId'),
                "pageNumber": pageNumber,
                "limit": limit,
                "keyword": null
            }

            axios.post(baseUrl + '/class/list', classDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
                .then(response => {
                    console.log(response);
                    if (response.data.success === true) {
                        setclassListData(response.data.data);
                    }
                    else {
                        setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                        setclassListData(null);
                        handleClick();
                    }
                })
                .catch(error => {
                    console.log(error, error.response, error.message, error.request);
                    setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                    setclassListData(null);
                    handleClick();
                })
        }
        else {

            history.push(
                {
                    pathname: "/dashboard"
                }
            )

        }

    }, []);

    const gethomeWorkData = (e) => {
        sethomeWorkData({ ...homeWorkData, [e.target.name]: e.target.value });
        console.log(homeWorkData, homeWorkList);
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const getClassData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        if (classData.class !== '') {
            setclassData({ ...classData, class: e.target.value, section: '' });
            setsubjectData({ ...subjectData, subjectName: '' });
            setsectionDTO(null);
            setsubjectDTO(null);
        }
        else {
            setclassData({ ...classData, class: e.target.value });
        }
        setclassDTO(classListData.data[child.props.id]);
        console.log(classData, classDTO, sectionDTO, subjectDTO, homeWorkList);
    }

    const getSectionData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setclassData({ ...classData, section: e.target.value });
        setsectionDTO(classDTO.sections[child.props.id]);
        console.log(classData, classDTO, sectionDTO);
        subjectListingApi();
    }

    const subjectListingApi = () => {

        var subjectDto = {
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        if (location.state) {
            subjectDto.classId = location.state.classObj.id
        }
        else {
            subjectDto.classId = classDTO.id
        }

        axios.post(baseUrl + '/subject/list', subjectDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setsubjectNameList(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setsubjectNameList(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setsubjectNameList(null);
                handleClick();
            })

    }

    const getSubjectData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setsubjectData({ ...subjectData, subjectName: e.target.value });
        setsubjectDTO(subjectNameList.data[child.props.id]);
        console.log(subjectData, subjectDTO);
    }

    const fetchhomeWorkData = () => {

        var homeWorkDto = {};

        homeWorkDto.classId = classDTO.id;
        homeWorkDto.sectionId = sectionDTO.id;
        homeWorkDto.subjectId = subjectDTO.id;
        homeWorkDto.date = homeWorkDate;

        setcontentFileDataList([]);
        sethomeWorkContentData({ description: '', contentName: '', contentType: '' });
        setmediaFiles({});
        sethomeWorkList(null);

        axios.post(baseUrl + '/homeWork/institute/list', homeWorkDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    sethomeWorkList(response.data.data);
                    setcontentFileDataList(response.data.data.contents);
                    sethomeWorkContentData({ ...homeWorkContentData, description: response.data.data.description, contentName: '', contentType: '' });

                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    sethomeWorkList(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                sethomeWorkList(null);
                handleClick();
            })

    }

    const createOrEdithomeWork = () => {

        var homeWorkDto = {
            "classId": classDTO.id,
            "sectionId": sectionDTO.id,
            "subjectId": subjectDTO.id,
            "description": homeWorkContentData.description,
            "date": homeWorkDate,
            "contents": contentFileDataList
        }

        if (homeWorkList === null) {
            homeWorkDto.type = "CREATE";
            homeWorkDto.id = null;
        }
        else {
            homeWorkDto.type = "EDIT";
            homeWorkDto.id = homeWorkList.id;
        }

        let formData = new FormData();
        formData.append('homeWorkDTO', JSON.stringify(homeWorkDto));
        for (var key in mediaFiles) {
            console.log(key, mediaFiles[key]);
            formData.append(key, mediaFiles[key]);
        }

        console.log(formData, homeWorkDto, JSON.stringify(homeWorkDto));

        axios.post(baseUrl + '/homeWork', formData, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'content-type': 'multipart/form-data', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    setcontentFileDataList([]);
                    sethomeWorkContentData({ description: '', contentName: '', contentType: '' });
                    setmediaFiles({});
                    sethomeWorkList(null);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })

    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5"></div>
                    <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <h5 style={{ color: "#B99E01" }}><i style={{ fontSize: "25px" }} className="fa fa-tasks" aria-hidden="true"></i>&nbsp;&nbsp;HomeWork <span style={{ color: "black" }}>Details</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        <DatePicker
                            selected={homeWorkDate}
                            onChange={date => sethomeWorkDate(date)}
                            dateFormat="yyyy/MM/dd"
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                        /><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Class</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={classData.class}
                                onChange={(e, child) => getClassData(e, child)}
                            >
                                {
                                    classListData === null ? null :
                                        classListData.data.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name} >
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl><br /><br /><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            classData.class === '' ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Section</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={classData.section}
                                            onChange={(e, child) => getSectionData(e, child)}
                                        >
                                            {
                                                classDTO.sections === null ? null :
                                                    classDTO.sections.map((option, index) => (
                                                        <MenuItem id={index} key={option.id} value={option.name}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl><br /><br /><br /><br />
                                </div>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            classData.section === '' ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Subject</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={subjectData.subjectName}
                                            onChange={(e, child) => getSubjectData(e, child)}
                                        >
                                            {
                                                subjectNameList === null ? null :
                                                    subjectNameList.data.map((option, index) => (
                                                        <MenuItem id={index} key={option.id} value={option.name}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl><br /><br /><br /><br />
                                </div>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                        {
                            classData.class === '' ? null :
                                <div>
                                    <Button disabled={
                                        classData.class === '' ||
                                        classData.section === '' ||
                                        subjectData.subjectName === ''
                                    } onClick={fetchhomeWorkData} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button><br /><br />
                                </div>
                        }
                    </div>
                </div>

                {
                    subjectData.subjectName === '' ? null :

                        <div>
                            <div className="row">
                                <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
                                <div className="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                    <textarea rows="4" cols="50" name="description" value={homeWorkContentData.description} onChange={gethomeWorkContentData} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
                                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"><br />
                                    <TextField name="contentName" value={homeWorkContentData.contentName} onChange={gethomeWorkContentData} style={{ width: "100%" }} label="Content Name" /><br /><br />
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"><br />
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Content Type</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={homeWorkContentData.contentType}
                                            onChange={gethomeWorkContentData}
                                            inputProps={{
                                                name: "contentType"
                                            }}
                                        // name="instituteType"
                                        >
                                            {contentTypes.map((option, index) => (
                                                <MenuItem key={option.id} value={option.name}>
                                                    {option.name}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl><br /><br />
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"><br />
                                    <Button disabled={
                                        homeWorkContentData.contentName === '' ||
                                        homeWorkContentData.contentType === '' ||
                                        fileData === null
                                    } onClick={uploadSyllabusContent} style={{ fontSize: "100%" }} variant="contained" color="primary">Upload</Button>&nbsp;&nbsp;
                                        {
                                        editState.status === false ? null :
                                            <Button onClick={removeEditMode} style={{ fontSize: "70%" }} variant="contained" color="secondary">Remove EditMode</Button>
                                    }
                                </div>
                            </div>
                            {
                                homeWorkContentData.contentType === '' ? null :
                                    <div className="row">
                                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
                                        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6"><br /><br />
                                            <div className="custom-file mb-3">
                                                <input accept={acceptContentType} type="file" onChange={uploadFile}
                                                    className="custom-file-input" id="customFile" name="filename" />
                                                <label className="custom-file-label" htmlFor="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                            }
                            {
                                contentFileDataList.length === 0 ? null :
                                    <div className="row">
                                        <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                        <div className="col-xs-12 col-sm-12 col-md-10 col-lg-10"><br /><br />

                                            <table className="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Sl.No</th>
                                                        <th>Name</th>
                                                        <th>Type</th>
                                                        <th>Edit/View</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {
                                                        contentFileDataList.map((data, index) => (
                                                            <tr key={data.priority}>
                                                                <td>{data.priority}</td>
                                                                <td>{data.name}</td>
                                                                <td>{data.type}</td>
                                                                <td>
                                                                    {
                                                                        typeof data.value === "undefined" ? <span>NA</span> :
                                                                            <span>
                                                                                {
                                                                                    editState.status === true ? null :
                                                                                        <span>
                                                                                            <Button onClick={() => editMediaFiles(data, index)} style={{ fontSize: "70%" }} variant="contained" color="primary">Edit</Button>&nbsp;&nbsp;
                                                                        </span>
                                                                                }
                                                                                {
                                                                                    data.view === false ? <span>NA</span> :
                                                                                        <Button onClick={() => viewMediaFiles(data)} style={{ fontSize: "70%" }} variant="contained" color="secondary">View</Button>
                                                                                }
                                                                            </span>
                                                                    }
                                                                </td>
                                                            </tr>
                                                        ))}
                                                </tbody>
                                            </table>

                                        </div>
                                        <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                    </div>
                            }
                            {
                                contentFileDataList.length === 0 ? null :
                                    <div className="row">
                                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"><br /><br />
                                            <Button disabled={
                                                homeWorkContentData.description === ''
                                            } onClick={createOrEdithomeWork} style={{ fontSize: "100%" }} variant="contained" color="primary">
                                                {homeWorkList === null ? <span>Create</span> : <span>Update</span>}
                                            </Button>
                                        </div>
                                    </div>
                            }

                            {
                                homeWorkList === null ? (
                                    <div className="row">
                                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center"><br /><br />
                                            <h5>No HomeWork Available !!</h5>
                                        </div>
                                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                                    </div>
                                ) : null
                            }
                        </div>

                }

            </div>

            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default HomeWorkDashboard




