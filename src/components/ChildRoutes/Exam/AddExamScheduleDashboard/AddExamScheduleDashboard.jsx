import React, { useState, useEffect } from 'react';
import "./AddExamScheduleDashboard.css";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useHistory, useLocation } from "react-router-dom";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import Snackbar from '../../../SnackBar/SnackBar';

function AddExamScheduleDashboard() {

    let location = useLocation();
    let history = useHistory();

    const [examScheduleData, setexamScheduleData] = useState({
        displayName: '',
        totalMark: '',
        passMark: '',
        comments: '',
        subject: '',
        comments: '',
        date: new Date(),
        startTime: new Date(),
        endTime: new Date(),
    });

    const [subjectDTO, setsubjectDTO] = useState(null);
    const [subjectData, setsubjectData] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);

    useEffect(() => {
        console.log(history, location);

        var subjectDto = {
            "classId": location.state.classObj.id,
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        axios.post(baseUrl + '/subject/list', subjectDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setsubjectData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setsubjectData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setsubjectData(null);
                handleClick();
            })

        if (location.state.examScheduleType === 'EDIT') {

            var beginTime = location.state.data.startTime.split(":");

            var closeTime = location.state.data.endTime.split(":");

            console.log(beginTime, closeTime);

            var today = new Date();

            setexamScheduleData({
                ...examScheduleData,
                displayName: location.state.data.displayName,
                totalMark: location.state.data.totalMark,
                passMark: location.state.data.passMark,
                comments: location.state.data.comments,
                subject: location.state.data.subject.name,
                date: new Date(location.state.data.date),
                startTime: new Date(today.getFullYear(), today.getMonth(), today.getDate(), beginTime[0], beginTime[1]),
                endTime: new Date(today.getFullYear(), today.getMonth(), today.getDate(), closeTime[0], closeTime[1]),
            });
            setsubjectDTO(location.state.data.subject);
        }

    }, []);

    const getexamScheduleData = (e) => {
        setexamScheduleData({ ...examScheduleData, [e.target.name]: e.target.value });
        console.log(examScheduleData, subjectDTO);
    }

    const gettimetableSubjectData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setexamScheduleData({ ...examScheduleData, subject: e.target.value });
        setsubjectDTO(subjectData.data[child.props.id]);
        console.log(examScheduleData, subjectDTO);
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const addtimetableSubmit = () => {

        var examScheduleDto = {
            "id": location.state.data === null ? null : location.state.data.id,
            "examId": location.state.examObj.id,
            "classId": location.state.classObj.id,
            "sectionId": location.state.sectionObj.id,
            "totalMark": examScheduleData.totalMark,
            "passMark": examScheduleData.passMark,
            "subjectId": subjectDTO.id,
            "date": examScheduleData.date,
            "startTime": examScheduleData.startTime.getHours() + ":" + examScheduleData.startTime.getMinutes(),
            "endTime": examScheduleData.endTime.getHours() + ":" + examScheduleData.endTime.getMinutes(),
            "displayName": examScheduleData.displayName,
            "comments": examScheduleData.comments,
            "type": location.state.examScheduleType
        }

        console.log(examScheduleDto, examScheduleData);

        axios.post(baseUrl + '/examSchedule', examScheduleDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    setTimeout(function () {
                        history.push(
                            {
                                pathname: '/dashboard/examScheduleDashboard',
                            }
                        );
                    }, 2000);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })
    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <h5><span style={{ color: "#B99E01" }}>Exam : </span>{location.state.examObj.name}</h5><br />
                        <h5><span style={{ color: "#B99E01" }}>Class : </span>{location.state.classObj.name}</h5><br />
                        <h5><span style={{ color: "#B99E01" }}>Section: </span>{location.state.sectionObj.name}</h5>
                    </div>
                    <div style={{ backgroundColor: "#eeeeee", borderRadius: "4px" }} className="col-xs-12 col-sm-12 col-md-6 col-lg-6"><br />
                        <h5 style={{ color: "#B99E01", textAlign: "center" }}><i className="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Exam Schedule<span style={{ color: "black" }}>Details</span></h5><br />
                        <TextField name="displayName" value={examScheduleData.displayName} onChange={getexamScheduleData} style={{ width: "100%" }} label="Schedule Name" /><br /><br />
                        <TextField name="totalMark" value={examScheduleData.totalMark} onChange={getexamScheduleData} style={{ width: "100%" }} label="Total Mark" /><br /><br />
                        <TextField name="passMark" value={examScheduleData.passMark} onChange={getexamScheduleData} style={{ width: "100%" }} label="Pass Mark" /><br /><br />
                        <TextField name="comments" value={examScheduleData.comments} onChange={getexamScheduleData} style={{ width: "100%" }} label="Comments" /><br /><br />
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Subject</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={examScheduleData.subject}
                                onChange={(e, child) => gettimetableSubjectData(e, child)}
                            >
                                {
                                    subjectData === null ? null :
                                        subjectData.data.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl><br /><br />
                        <span>Date</span>
                        <div className="customDatePickerWidth">
                            <DatePicker
                                selected={examScheduleData.date}
                                onChange={date => setexamScheduleData({ ...examScheduleData, date: date })}
                                dateFormat="yyyy/MM/dd"
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                            /></div><br />
                        <span>Start Time</span>
                        <div className="customDatePickerWidth">
                            <DatePicker
                                selected={examScheduleData.startTime}
                                onChange={date => setexamScheduleData({ ...examScheduleData, startTime: date })}
                                showTimeSelect
                                showTimeSelectOnly
                                timeIntervals={15}
                                timeCaption="Time"
                                dateFormat="h:mm aa"
                            /></div><br />
                        <span>Close Time</span>
                        <div className="customDatePickerWidth">
                            <DatePicker
                                selected={examScheduleData.endTime}
                                onChange={date => setexamScheduleData({ ...examScheduleData, endTime: date })}
                                showTimeSelect
                                showTimeSelectOnly
                                timeIntervals={15}
                                timeCaption="Time"
                                dateFormat="h:mm aa"
                            /></div>
                        <br />
                        <Button disabled={
                            examScheduleData.displayName === '' ||
                            examScheduleData.subject === ''
                        } onClick={addtimetableSubmit} style={{ fontSize: "100%" }} variant="contained" color="primary">Submit</Button><br /><br />
                    </div>
                </div>
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default AddExamScheduleDashboard



