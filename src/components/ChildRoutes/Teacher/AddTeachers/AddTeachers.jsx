import React, { useState, useEffect } from 'react';
import "./AddTeachers.css";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useHistory, useLocation } from "react-router-dom";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import Switch from '@material-ui/core/Switch';
import Snackbar from '../../../SnackBar/SnackBar';

function AddTeachers() {

    let location = useLocation();
    let history = useHistory();

    const [teachersData, setteachersData] = useState({
        firstName: '',
        lastName: '',
        regNo: '',
        mobileNumber: '',
        address: '',
        autoReg: true,
        DOB: new Date(),
        gender: 'Male'
    });

    const [uploadImage, setuploadImage] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    useEffect(() => {
        console.log(history, location);
        if (location.state.teacherType === 'EDIT') {
            setteachersData({
                ...teachersData,
                firstName: location.state.data.firstName,
                lastName: location.state.data.lastName,
                mobileNumber: location.state.data.mobileNumber,
                address: location.state.data.address,
                autoReg: location.state.data.autoGenerate,
                DOB: new Date(location.state.data.dateOfBirth),
                gender: location.state.data.gender
            });
            setuploadImage(location.state.data.imageUrl);
        }
    }, []);

    const getteachersData = (e) => {
        setteachersData({ ...teachersData, [e.target.name]: e.target.value });
        console.log(teachersData, uploadImage);
    }

    const getUploadedImage = (e) => {
        console.log(e, e.target.files);
        setuploadImage(e.target.files[0]);
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const addTeachersSubmit = () => {

        var teacherDto = {
            "firstName": teachersData.firstName,
            "lastName": teachersData.lastName,
            "address": teachersData.address,
            "mobileNumber": teachersData.mobileNumber,
            "autoGenerate": teachersData.autoReg,
            "gender": teachersData.gender,
            "dateOfBirth": teachersData.DOB.getFullYear() + "-" + (teachersData.DOB.getMonth() + 1) + "-" + teachersData.DOB.getDate(),
            "instituteId": localStorage.getItem('InstituteId'),
            "id": location.state.data === null ? null : location.state.data.id,
            "type": location.state.teacherType
        }

        let formData = new FormData();
        formData.append('teacherDto', JSON.stringify(teacherDto));
        formData.append('image', uploadImage);

        console.log(formData, teacherDto, JSON.stringify(teachersData), uploadImage);

        axios.post(baseUrl + '/teacher', formData, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'content-type': 'multipart/form-data', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    setTimeout(function () {
                        history.push(
                            {
                                pathname: '/dashboard/institute/teacher',
                            }
                        );
                    }, 2000);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })
    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div style={{ backgroundColor: "#eeeeee", borderRadius: "4px" }} className="col-xs-12 col-sm-12 col-md-6 col-lg-6"><br />
                        <h5 style={{ color: "#B99E01", textAlign: "center" }}><i className="fas fa-chalkboard-teacher"></i>&nbsp;&nbsp;Teacher<span style={{ color: "black" }}>Details</span></h5><br />
                        <TextField name="firstName" value={teachersData.firstName} onChange={getteachersData} style={{ width: "100%" }} label="First Name" /><br /><br />
                        <TextField name="lastName" value={teachersData.lastName} onChange={getteachersData} style={{ width: "100%" }} label="Last Name" /><br /><br />
                        {/* <TextField name="regNo" value={teachersData.regNo} onChange={getteachersData} style={{ width: "100%" }} label="Reg No" /><br /><br /> */}
                        <TextField name="mobileNumber" value={teachersData.mobileNumber} onChange={getteachersData} style={{ width: "100%" }} label="Mobile No" /><br /><br />
                        <TextField name="address" value={teachersData.address} onChange={getteachersData} style={{ width: "100%" }} label="Address" /><br /><br />
                        <Switch
                            checked={teachersData.autoReg}
                            onChange={e => setteachersData({ ...teachersData, autoReg: e.target.checked })}
                            color="primary"
                            name="autoReg"
                            inputProps={{ 'aria-label': 'primary checkbox' }}
                        />
                        {teachersData.autoReg === true ? <span>AutoReg</span> : <TextField name="regNo" value={teachersData.regNo} onChange={getteachersData} style={{ width: "100%" }} label="RegNo" />}<br /><br />
                        <div className="customDatePickerWidth">
                            <DatePicker dateFormat="yyyy/MM/dd" showMonthDropdown showYearDropdown selected={teachersData.DOB} onChange={date => setteachersData({ ...teachersData, DOB: date })} /></div><br />
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Gender</FormLabel>
                            <RadioGroup row aria-label="gender" name="gender" value={teachersData.gender} onChange={getteachersData}>
                                <FormControlLabel value="Male" control={<Radio color="primary" />} label="Male" />
                                <FormControlLabel value="Female" control={<Radio color="primary" />} label="Female" />
                                <FormControlLabel value="Other" control={<Radio color="primary" />} label="Other" />
                            </RadioGroup>
                        </FormControl><br /><br />
                        <Button
                            variant="contained"
                            component="label"
                            color="secondary"
                        >
                            Upload Image
                            <input
                                type="file"
                                style={{ display: "none" }}
                                onChange={getUploadedImage}
                            />
                        </Button>
                        {uploadImage !== null ? <span>&nbsp;&nbsp;<i style={{ color: "green" }} className="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Successfully Uploaded !!</span> : null}
                        <br /><br />
                        <Button disabled={
                            teachersData.firstName === '' ||
                            teachersData.lastName === '' ||
                            teachersData.address === '' ||
                            teachersData.mobileNumber === '' ||
                            uploadImage === null
                        } onClick={addTeachersSubmit} style={{ fontSize: "100%" }} variant="contained" color="primary">Submit</Button><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default AddTeachers
