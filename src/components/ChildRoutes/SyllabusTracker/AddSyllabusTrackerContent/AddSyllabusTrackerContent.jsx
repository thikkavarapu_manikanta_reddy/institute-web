import React, { useState, useEffect } from 'react';
import "./AddSyllabusTrackerContent.css";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useHistory, useLocation } from "react-router-dom";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import $ from 'jquery';

function AddSyllabusTrackerContent() {

    const contentTypes = [
        {
            "id": "1",
            "name": "IMAGE"
        },
        {
            "id": "2",
            "name": "AUDIO"
        },
        {
            "id": "3",
            "name": "VIDEO"
        },
        {
            "id": "4",
            "name": "PDF"
        }
    ];

    let location = useLocation();
    let history = useHistory();

    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    const [syllabusContentData, setsyllabusContentData] = useState({
        contentName: '',
        contentType: ''
    });

    const [contentFileDataList, setcontentFileDataList] = useState([]);

    const [mediaFiles, setmediaFiles] = useState({});

    const [fileData, setfileData] = useState(null);

    const [editState, seteditState] = useState({
        status: false,
        index: -1
    });

    const [acceptContentType, setacceptContentType] = useState(".jpeg,.jpg,.png,.mp4,.mp3,.pdf");

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    const getsyllabusContentData = (e) => {
        setsyllabusContentData({ ...syllabusContentData, [e.target.name]: e.target.value });
        console.log(e.target.value, e.target.name);
        if (e.target.name === "contentType") {
            if (e.target.value === "IMAGE") {
                setacceptContentType(".jpeg,.jpg,.png");
            }
            else if (e.target.value === "AUDIO") {
                setacceptContentType(".mp3,.wav");
            }
            else if (e.target.value === "VIDEO") {
                setacceptContentType(".mp4");
            }
            else {
                setacceptContentType(".pdf");
            }
        }
        setTimeout(function () { setsyllabusContentData(prevSyllabusContentData => ({ ...prevSyllabusContentData, contentName: prevSyllabusContentData.contentName, contentType: prevSyllabusContentData.contentType })); }, 0);
        console.log(syllabusContentData, contentFileDataList, mediaFiles, fileData, acceptContentType);
    }

    const uploadSyllabusContent = () => {

        if (editState.status === false) {
            setcontentFileDataList([...contentFileDataList, {
                "priority": contentFileDataList.length + 1,
                "name": syllabusContentData.contentName,
                "type": syllabusContentData.contentType,
                "dtoType": "CREATE",
                "view": true,
                [location.state.data.title + "-" + syllabusContentData.contentName]: fileData
            }]);
            console.log(contentFileDataList.length + 1, contentFileDataList.length);
            if (location.state.data.trackerType === "CHAPTER")
                setmediaFiles({ ...mediaFiles, ["CHAPTER-" + location.state.data.priority + "-CONTENT-" + (contentFileDataList.length + 1)]: fileData });
            else
                setmediaFiles({ ...mediaFiles, ["REFERENCE-" + location.state.data.priority + "-CONTENT-" + (contentFileDataList.length + 1)]: fileData });

        }
        else {
            let newArr = [...contentFileDataList];
            newArr[editState.index].name = syllabusContentData.contentName;
            newArr[editState.index].type = syllabusContentData.contentType;
            newArr[editState.index].view = false;
            setcontentFileDataList(newArr);
            seteditState({ ...editState, status: false, index: -1 });

            if (location.state.data.trackerType === "CHAPTER")
                setmediaFiles({ ...mediaFiles, ["CHAPTER-" + location.state.data.priority + "-CONTENT-" + newArr[editState.index].priority]: fileData });
            else
                setmediaFiles({ ...mediaFiles, ["REFERENCE-" + location.state.data.priority + "-CONTENT-" + newArr[editState.index].priority]: fileData });
        }

        setfileData(null);

        setsyllabusContentData({ ...syllabusContentData, contentName: '', contentType: '' });

        console.log(contentFileDataList, mediaFiles);

    }

    const updateContentForChapter = () => {

        location.state.data.contents = contentFileDataList;

        console.log(location.state.data);

        history.push(
            {
                pathname: "/dashboard/syllabusTrackerDashboard",
                state: {
                    data: location.state.data,
                    chapterList: location.state.chapterList,
                    referenceList: location.state.referenceList,
                    classObj: location.state.classObj,
                    sectionObj: location.state.sectionObj,
                    subjectObj: location.state.subjectObj,
                    classListData: location.state.classListData,
                    subjectNameList: location.state.subjectNameList,
                    mediaFiles: mediaFiles,
                    syllabusTrackerList: location.state.syllabusTrackerList
                }
            }
        )

    }

    const uploadFile = (e) => {

        console.log(e.target.files, "cool", e.target.files[0].name);

        setfileData(e.target.files[0]);

        console.log(fileData);

    }

    const viewMediaFiles = (data) => {
        window.open(data.value);
    }

    const editMediaFiles = (data, index) => {

        setsyllabusContentData({ ...syllabusContentData, contentName: data.name, contentType: data.type });
        seteditState({ ...editState, status: true, index: index });

        if (data.type === "IMAGE") {
            setacceptContentType(".jpeg,.jpg,.png");
        }
        else if (data.type === "AUDIO") {
            setacceptContentType(".mp3,.wav");
        }
        else if (data.type === "VIDEO") {
            setacceptContentType(".mp4");
        }
        else {
            setacceptContentType(".pdf");
        }

        setTimeout(function () { setsyllabusContentData(prevSyllabusContentData => ({ ...prevSyllabusContentData, contentName: prevSyllabusContentData.contentName, contentType: prevSyllabusContentData.contentType })); }, 0);

    }

    const removeEditMode = () => {
        seteditState({ ...editState, status: false, index: -1 });
        setfileData(null);
        setsyllabusContentData({ ...syllabusContentData, contentName: '', contentType: '' });
        console.log(contentFileDataList, mediaFiles);
    }

    useEffect(() => {
        console.log(history, location);

        if (location.state.data.contents.length !== 0) {
            setcontentFileDataList(location.state.data.contents);
        }
        if (location.state.mediaFiles.length !== 0) {
            setmediaFiles(location.state.mediaFiles);
        }

    }, []);

    return (
        <div>
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6"><br />
                    <h5 style={{ color: "#B99E01", textAlign: "center" }}><i className="fa fa-calendar"></i>&nbsp;&nbsp;Syllabus<span style={{ color: "black" }}>Details</span></h5><br />
                </div>
                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
            </div>
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                <div className="col-xs-12 col-sm-12 col-md-11 col-lg-11"><br />
                    <h5><span style={{ color: "#B99E01" }}>Chapter : </span>{location.state.data.title}</h5>
                </div>
            </div>
            {
                roleStatus !== 'STUDENT' ?
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"><br />
                            <TextField name="contentName" value={syllabusContentData.contentName} onChange={getsyllabusContentData} style={{ width: "100%" }} label="Content Name" /><br /><br />
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"><br />
                            <FormControl style={{ width: "100%" }}>
                                <InputLabel id="demo-simple-select-label">Content Type</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={syllabusContentData.contentType}
                                    onChange={getsyllabusContentData}
                                    inputProps={{
                                        name: "contentType"
                                    }}
                                // name="instituteType"
                                >
                                    {contentTypes.map((option, index) => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl><br /><br />
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"><br />
                            <Button disabled={
                                syllabusContentData.contentName === '' ||
                                syllabusContentData.contentType === '' ||
                                fileData === null
                            } onClick={uploadSyllabusContent} style={{ fontSize: "100%" }} variant="contained" color="primary">Upload</Button>&nbsp;&nbsp;
                                                                    {
                                editState.status === false ? null :
                                    <Button onClick={removeEditMode} style={{ fontSize: "70%" }} variant="contained" color="secondary">Remove EditMode</Button>
                            }
                        </div>
                    </div>
                    : null
            }
            {
                syllabusContentData.contentType === '' ? null :
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6"><br /><br />
                            <div className="custom-file mb-3">
                                <input accept={acceptContentType} type="file" onChange={uploadFile}
                                    className="custom-file-input" id="customFile" name="filename" />
                                <label className="custom-file-label" htmlFor="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
            }
            {
                contentFileDataList.length === 0 ? null :
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                        <div className="col-xs-12 col-sm-12 col-md-10 col-lg-10"><br /><br />

                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                        {
                                            roleStatus !== 'STUDENT' ?
                                                <th>Edit/View</th> : <th>View</th>
                                        }
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        contentFileDataList.map((data, index) => (
                                            <tr key={data.priority}>
                                                <td>{data.priority}</td>
                                                <td>{data.name}</td>
                                                <td>{data.type}</td>
                                                <td>
                                                    {
                                                        typeof data.value === "undefined" ? <span>NA</span> :
                                                            <span>
                                                                {
                                                                    editState.status === true ? null :
                                                                        <span>
                                                                            {
                                                                                roleStatus !== 'STUDENT' ?
                                                                                    <span>
                                                                                        <Button onClick={() => editMediaFiles(data, index)} style={{ fontSize: "70%" }} variant="contained" color="primary">Edit</Button>&nbsp;&nbsp;
                                                                                    </span>
                                                                                    : null
                                                                            }
                                                                        </span>
                                                                }
                                                                {
                                                                    data.view === false ? <span>NA</span> :
                                                                        <Button onClick={() => viewMediaFiles(data)} style={{ fontSize: "70%" }} variant="contained" color="secondary">View</Button>
                                                                }
                                                            </span>
                                                    }
                                                </td>
                                            </tr>
                                        ))}
                                </tbody>
                            </table>

                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    </div>
            }
            {
                contentFileDataList.length === 0 ? null :
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"><br /><br />
                            {
                                roleStatus !== 'STUDENT' ?
                                    <div>
                                        <Button onClick={updateContentForChapter} style={{ fontSize: "100%" }} variant="contained" color="primary">Update</Button>
                                    </div>
                                    : null
                            }
                        </div>
                    </div>
            }
        </div>
    )
}

export default AddSyllabusTrackerContent
