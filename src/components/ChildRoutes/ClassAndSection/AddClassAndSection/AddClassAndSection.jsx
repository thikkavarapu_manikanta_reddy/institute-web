import React, { useState, useEffect } from 'react';
import "./AddClassAndSection.css";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useHistory, useLocation } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import Snackbar from '../../../SnackBar/SnackBar';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import { Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';

function AddClassAndSection() {

    let location = useLocation();
    let history = useHistory();

    const [classData, setclassData] = useState({
        className: '',
        sectionName: ''
    });

    const [sectionNameList, setsectionNameList] = useState([]);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    const [section, setsection] = useState(false);

    const [editSection, seteditSection] = useState(false);

    const [editSectionField, seteditSectionField] = useState('');

    const [editSectionFieldIndex, seteditSectionFieldIndex] = useState('');

    useEffect(() => {
        console.log(history, location);
        if (location.state.classType === 'EDIT') {
            setclassData({
                ...classData,
                className: location.state.data.name,
                sectionName: ''
            });
            setsectionNameList(location.state.data.sections);
            console.log(sectionNameList);
        }
    }, []);

    const getclassData = (e) => {
        setclassData({ ...classData, [e.target.name]: e.target.value });
        console.log(classData, sectionNameList);
    }

    const addSections = () => {
        setsection(true);
    }

    const addSectionList = () => {
        setsectionNameList([...sectionNameList, {
            id: sectionNameList.length,
            name: classData.sectionName,
            type: "CREATE"
        }]);
        setclassData({ ...classData, sectionName: '' });
        setsection(false);
        console.log(classData, sectionNameList);
    }

    const editSectionList = () => {
        console.log(editSectionField);
        let newArr = [...sectionNameList];
        newArr[editSectionFieldIndex].name = editSectionField.name;
        setsectionNameList(newArr);
        toggleSectionModal();
        console.log(sectionNameList);
    }

    const editSectionFieldHandler = (e) => {
        seteditSectionField({ ...editSectionField, name: e.target.value });
        console.log(editSectionField);
    };

    const editSections = (section,index) => {
        seteditSectionFieldIndex(index);
        seteditSectionField(section);
        seteditSection(true);
    };

    const toggleSectionModal = () => {
        seteditSection(!editSection);
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const addclassSubmit = () => {

        var classDto = {
            "name": classData.className,
            "sections": sectionNameList,
            "id": location.state.data === null ? null : location.state.data.id,
            "instituteId": localStorage.getItem('InstituteId'),
            "type": location.state.classType
        }

        console.log(classDto);

        axios.post(baseUrl + '/class', classDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    setTimeout(function () {
                        history.push(
                            {
                                pathname: '/dashboard/institute/class',
                            }
                        );
                    }, 2000);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })
    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        <h5 style={{ color: "#B99E01" }}><i className="fas fa-chalkboard-class"></i>&nbsp;&nbsp;Class<span style={{ color: "black" }}>Details</span></h5><br />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5"><br />
                        <TextField name="className" value={classData.className} onChange={getclassData} style={{ width: "70%" }} label="Class Name" />
                        <Button disabled={
                            classData.className === '' ||
                            sectionNameList.length === 0
                        } onClick={addclassSubmit} style={{ fontSize: "100%" }} variant="contained" color="primary">Submit</Button>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    <div style={{ borderRadius: "4px" }} className="col-xs-12 col-sm-12 col-md-5 col-lg-5"><br />
                        <Fab onClick={addSections} size="small" color="primary" aria-label="add">
                            <AddIcon />
                        </Fab>&nbsp;&nbsp;Add Section<br /><br />
                        {section === false ? null :
                            <div>
                                <TextField name="sectionName" value={classData.sectionName} onChange={getclassData} style={{ width: "60%" }} label="Section Name" />
                                <Button disabled={
                                    classData.sectionName === ''
                                } onClick={addSectionList} style={{ fontSize: "100%" }} variant="contained" color="secondary">Add</Button>
                            </div>
                        }<br /><br />
                    </div>
                </div>
                <div className="row">
                    {sectionNameList.map((data, index) => (
                        <div key={data.id} className="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center"><br /><br />
                            <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            {data.name}
                                        </div>
                                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                            <Fab onClick={() => editSections(data,index)} color="secondary" size="small" aria-label="edit">
                                                <EditIcon />
                                            </Fab>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>

            {/* ModalSection */}
            <Modal style={{marginTop:"200px"}} className="my-modal" isOpen={editSection} toggle={toggleSectionModal}>
                <ModalHeader cssModule={{ 'modal-title': 'w-100 text-center' }} toggle={toggleSectionModal}>
                    <Button style={{ fontSize: "60%" }} variant="contained" color="primary">Section</Button>
                </ModalHeader>
                <ModalBody>
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                        <div className="col-xs-12 col-sm-12 col-md-7 col-lg-7 text-center">
                        <TextField value={editSectionField.name} onChange={editSectionFieldHandler} style={{ width: "60%" }} label="Section Name" />
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-center">
                            <Button disabled={editSectionField.name === ''} onClick={editSectionList} style={{ fontSize: "90%" }} variant="contained" color="primary">Edit</Button>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button style={{ fontSize: "70%" }} variant="contained" color="secondary" onClick={toggleSectionModal}>Cancel</Button>
                </ModalFooter>
            </Modal>

            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default AddClassAndSection

