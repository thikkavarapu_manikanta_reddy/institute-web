import React, { useState, useEffect } from 'react';
import "./TimetableDashboard.css";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import { useHistory, useLocation } from "react-router-dom";
import Snackbar from '../../../SnackBar/SnackBar';
import Avatar from 'react-avatar';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import StepProgress from '../../../StepProgress/StepProgress';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography variant="h5" component="h5">{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
}));

function TimetableDashboard() {

    let history = useHistory();
    let location = useLocation();
    const classes = useStyles();

    const workingDays = [
        {
            id: 1,
            name: 'Monday'
        },
        {
            id: 2,
            name: 'Tuesday'
        },
        {
            id: 3,
            name: 'Wednesday'
        },
        {
            id: 4,
            name: 'Thursday'
        },
        {
            id: 5,
            name: 'Friday'
        },
        {
            id: 6,
            name: 'Saturday'
        },
        {
            id: 7,
            name: 'Sunday'
        }
    ];

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);
    const [timetableData, settimetableData] = useState(null);
    const [classListData, setclassListData] = useState(null);

    const [dayIndex, setdayIndex] = React.useState(0);

    const [classData, setclassData] = useState({
        class: '',
        section: ''
    });

    const [classDTO, setclassDTO] = useState(null);
    const [sectionDTO, setsectionDTO] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: false });

    const [open, setOpen] = React.useState(false);

    const [periodDay, setperiodDay] = useState('');

    const [periodDayTimetable, setperiodDayTimetable] = useState(null);

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    useEffect(() => {

        var classDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        axios.post(baseUrl + '/class/list', classDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setclassListData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setclassListData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setclassListData(null);
                handleClick();
            })

    }, []);

    const getClassData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        if (classData.class !== '') {
            setclassData({ ...classData, class: e.target.value, section: '' });
            setsectionDTO(null);
        }
        else {
            setclassData({ ...classData, class: e.target.value });
        }
        setclassDTO(classListData.data[child.props.id]);
        console.log(classData, classDTO, sectionDTO);
    }

    const getSectionData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setclassData({ ...classData, section: e.target.value });
        setsectionDTO(classDTO.sections[child.props.id]);
        console.log(classData, classDTO, sectionDTO);
    }


    const fetchTimetable = () => {

        var timetableDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "classId": classDTO.id,
            "sectionId": sectionDTO.id
        }

        console.log(timetableDto);

        axios.post(baseUrl + '/classSchedule/list', timetableDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    settimetableData(response.data.data);
                    getPeriodDay("Monday", response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    settimetableData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                settimetableData(null);
                handleClick();
            })

    }

    const addOrEditTimetable = (status, data, classListData) => {

        history.push(
            {
                pathname: "/dashboard/institute/timetable/add",
                state: {
                    studentType: status,
                    data: data,
                    classListData: classListData,
                    classObj: classDTO,
                    sectionObj: sectionDTO
                }
            }
        )
    }

    const handleChange = (event, newValue) => {
        console.log(newValue);
        setdayIndex(newValue);
        getPeriodDay(workingDays[newValue].name, timetableData);
    };

    const getPeriodDay = (day, data) => {
        console.log(day, data);
        setperiodDay(day);
        var exist = true;
        Object.entries(data.classSchedule).map(([key, value]) => {
            if (key === day) {
                console.log(key, value, data.classSchedule[key]);
                setperiodDayTimetable(data.classSchedule[key]);
                exist = false;
                return true;
            }
        }
        );
        if (exist === true) {
            setperiodDayTimetable(null);
        }
    };

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <div>
            <StepProgress /><br /><br />
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <h5 style={{ color: "#B99E01" }}><i style={{ fontSize: "20px" }} className="fa fa-calendar"></i>&nbsp;&nbsp;Timetable<span style={{ color: "black" }}>Dashboard</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Class</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={classData.class}
                                onChange={(e, child) => getClassData(e, child)}
                            >
                                {
                                    classListData === null ? null :
                                        classListData.data.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name} >
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl><br /><br /><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            classData.class === '' ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Section</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={classData.section}
                                            onChange={(e, child) => getSectionData(e, child)}
                                        >
                                            {
                                                classDTO.sections === null ? null :
                                                    classDTO.sections.map((option, index) => (
                                                        <MenuItem id={index} key={option.id} value={option.name}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl><br /><br /><br /><br />
                                </div>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        {
                            classData.class === '' ? null :
                                <div>
                                    <Button disabled={
                                        classData.class === '' ||
                                        classData.section === ''
                                    } onClick={fetchTimetable} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button><br /><br />
                                </div>
                        }
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                        {
                            classData.section === '' ? null :
                                <div>
                                    {
                                        roleStatus === 'INSTITUTE-ADMIN' ?
                                            <div>
                                                <Fab onClick={() => addOrEditTimetable('CREATE', null, classListData)} size="small" color="primary" aria-label="add">
                                                    <AddIcon />
                                                </Fab>&nbsp;&nbsp;Add timetable<br /><br /><br />
                                            </div>
                                            : null
                                    }
                                </div>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            timetableData === null ? (
                                <h5>No timetable Available !!</h5>
                            ) : null
                            // (
                            //         <div>
                            //             <FormControl style={{ width: "100%" }}>
                            //                 <InputLabel id="demo-simple-select-label">Period Day</InputLabel>
                            //                 <Select
                            //                     labelId="demo-simple-select-label"
                            //                     id="demo-simple-select"
                            //                     value={periodDay}
                            //                     onChange={getPeriodDay}
                            //                 >
                            //                     {workingDays.map((option, index) => (
                            //                         <MenuItem key={option.id} value={option.name}>
                            //                             {option.name}
                            //                         </MenuItem>
                            //                     ))}
                            //                 </Select>
                            //             </FormControl>
                            //         </div>
                            //     )
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                </div>
                {
                    timetableData === null ? null :
                        <div className={classes.root}>
                            <AppBar position="static">
                                <Tabs
                                    value={dayIndex}
                                    onChange={handleChange}
                                    variant="scrollable"
                                    scrollButtons="auto"
                                    aria-label="scrollable auto tabs example"
                                >
                                    {
                                        workingDays.map((option, index) => (
                                            <Tab key={index} label={option.name} {...a11yProps(index)} />
                                        ))
                                    }
                                </Tabs>
                            </AppBar>
                            {
                                workingDays.map((option, index) => (
                                    <TabPanel key={index} value={dayIndex} index={index}>
                                        <div className="row">
                                            {
                                                timetableData === null ? null :
                                                    periodDayTimetable === null ?
                                                        (
                                                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                                                No Periods Available !!
                                                            </div>
                                                        ) :
                                                        periodDayTimetable.map((data, index) => (
                                                            <div key={data.id} className="col-xs-12 col-sm-12 col-md-12 col-lg-6"><br /><br />
                                                                <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                                                    <div className="card-body">
                                                                        <div className="row">
                                                                            <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                                                                <Avatar name={data.displayName} size="35" round={true} />
                                                                            </div>
                                                                            <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                                                <h5 style={{ marginTop: "4px" }} className="card-title">{data.displayName}</h5>
                                                                            </div>
                                                                            <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                                                {
                                                                                    roleStatus === 'INSTITUTE-ADMIN' ?
                                                                                        <div>
                                                                                            <Fab onClick={() => addOrEditTimetable('EDIT', data, classListData)} color="secondary" size="small" aria-label="edit">
                                                                                                <EditIcon />
                                                                                            </Fab>
                                                                                        </div>
                                                                                        : null
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                                                            <div className="col-xs-12 col-sm-12 col-md-11 col-lg-11">
                                                                                <h5>Subject Name : {data.subjectDTO.name}</h5>
                                                                                <h5>Teacher Name : {data.teacherDTO.firstName + " " + data.teacherDTO.lastName}</h5>
                                                                                <h5>Timings : {data.displayStartTime + "-" + data.displayEndTime}</h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><br />
                                                            </div>
                                                        ))}
                                        </div>
                                    </TabPanel>
                                ))
                            }
                        </div>
                }
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div >
    )
}

export default TimetableDashboard


