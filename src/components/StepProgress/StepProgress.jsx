import React, { useState, useEffect } from 'react';
import "./StepProgress.css";
import axios from 'axios';
import { baseUrl } from '../ServerUrls';
import Snackbar from '../SnackBar/SnackBar';
import { useHistory, useLocation } from "react-router-dom";

import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    button: {
        marginRight: theme.spacing(1),
    },
    completed: {
        display: 'inline-block',
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
}));

function getSteps() {
    return ['Institute Creation', 'Teacher Creation', 'Class and section Creation', 'Student Creation', 'Timetable Creation'];
}

function StepProgress() {

    const classes = useStyles();
    let history = useHistory();
    let location = useLocation();

    console.log(location.pathname);

    var stepIndex = 0;
    var currentStep = location.pathname.split("/")[3];

    console.log(stepIndex, currentStep);

    if (currentStep === "onBoard")
        stepIndex = 0;
    else if (currentStep === "teacher")
        stepIndex = 1;
    else if (currentStep === "class")
        stepIndex = 2;
    else if (currentStep === "student")
        stepIndex = 3;
    else if (currentStep === "timetable")
        stepIndex = 4;
    else
        stepIndex = -1;

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    const [steprogress, setsteprogress] = useState(null);

    const [activeStep, setActiveStep] = useState(stepIndex);
    const [completed, setCompleted] = useState({});
    const steps = getSteps();


    const initialHandleStep = (step) => {
        setActiveStep(step);
        console.log(step, activeStep);
    }

    const handleStep = (step) => {
        setActiveStep(step);
        console.log(step, activeStep);
        if (step === 0) {
            history.push(
                {
                    pathname: '/dashboard/institute/onBoard'
                }
            )
        }
        else if (step === 1) {
            history.push(
                {
                    pathname: '/dashboard/institute/teacher'
                }
            )
        }
        else if (step === 2) {
            history.push(
                {
                    pathname: '/dashboard/institute/class'
                }
            )
        }
        else if (step === 3) {
            history.push(
                {
                    pathname: '/dashboard/institute/student'
                }
            )
        }
        else if (step === 4) {
            history.push(
                {
                    pathname: '/dashboard/institute/timetable'
                }
            )
        }
    };

    const handleComplete = (step) => {
        const newCompleted = completed;
        newCompleted[step] = true;
        setCompleted(newCompleted);
        console.log(step, completed);
    };

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    useEffect(() => {

        console.log(history, location);
        let isSubscribed = true;

        axios.get(baseUrl + '/metaData/stepProgress', { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (isSubscribed === true) {
                    if (response.data.success === true) {
                        setsteprogress(response.data.data);
                        console.log(response.data.data);
                        for (var i = 0; i < response.data.data.stepProgressList.length; i++) {
                            var status = response.data.data.stepProgressList[i].status;
                            if (status === "COMPLETED") {
                                console.log(i);
                                setActiveStep(i);
                                handleComplete(i);
                            }
                            if (i === response.data.data.stepProgressList.length - 1) {
                                console.log(i, "Cool");
                                initialHandleStep(stepIndex);
                            }
                        }
                    }
                    else {
                        setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                        setsteprogress(null);
                        handleClick();
                    }
                }
            })
            .catch(error => {
                if (isSubscribed === true) {
                    console.log(error, error.response, error.message, error.request);
                    setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                    setsteprogress(null);
                    handleClick();
                }
            })
        return () => (isSubscribed = false);
    }, []);

    return (
        <div>

            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}

            <div className={classes.root}>
                <Stepper alternativeLabel nonLinear activeStep={activeStep}>
                    {steps.map((label, index) => (
                        <Step key={label}>
                            <StepButton onClick={() => handleStep(index)} completed={completed[index]}>
                                {label}
                            </StepButton>
                        </Step>
                    ))}
                </Stepper>
            </div>
        </div>
    )
}

export default StepProgress
