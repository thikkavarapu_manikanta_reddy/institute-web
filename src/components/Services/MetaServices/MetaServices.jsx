import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { baseUrl } from '../../ServerUrls';

function MetaServices(props) {

    const [metaData, setMetaData] = useState({});

    useEffect(() => {
        let isSubscribed = true;
        axios.get(baseUrl + '/metaData', { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (isSubscribed === true) {
                    if (response.data.success === true) {
                        setMetaData(response.data.data);
                    }
                }
            })
            .catch(error => {
                if (isSubscribed === true) {
                    console.log(error, error.response, error.message, error.request);
                }
            })
        return () => (isSubscribed = false);
    }, []);

    return (
        <div>
            {props.children(metaData)}
        </div>
    )
}

export default MetaServices

